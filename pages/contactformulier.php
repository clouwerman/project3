<?php

use     PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once('../config.php');

require_once('../vendor/autoload.php');

$sentMail = false;

if (isset($_POST['submit']))
{
    if ( ! isset($_POST['email']))
    {
        die('Email niet juist ingevoerd.');
    }

    $email = $_POST['email'];

    if ( ! isset($_POST['message']))
    {
        die('Bericht is niet meegegeven.');
    }

    $message = $_POST['message'];

    $uip = $_SERVER['REMOTE_ADDR'];

    $date = time();

    // Kijkt of het geen spam is
    $query = "SELECT * FROM messages WHERE uip='$uip' AND date > $date - 86400";
    $result = mysqli_query($con, $query);
    if (mysqli_num_rows($result) >= 1000)
    {
        die('Je hebt vandaag al 3 keer een bericht gestuurd, even dimmen maloot.');
    }

    $query = "INSERT INTO messages (message,uip,email,date) VALUES ('$message','$uip','$email',$date)";
    if ( ! mysqli_query($con, $query))
    {
        die('Kon bericht niet versturen, contacteer op: cpm.louwerman@student.alfa-college.nl!');
    }

    if ( ! $debug)
    {
        $mail = new PHPMailer(true);

        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = $smtpHost;
        $mail->SMTPAuth = true;
        $mail->Username = $smtpUsername;
        $mail->Password = $smtpPassword;
        $mail->SMTPSecure = $smtpSecure;
        $mail->Port = $smtpPort;

        $mail->setFrom($fromEmail);
        $mail->addAddress('clouwerman@gmail.com');

        $mail->isHTML(true);
        $mail->Subject = 'Er is een contact aanvraag gedaan via: ' . $email;
        $mail->Body = $message;

        $mail->send();
    }

    $sentMail = true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Al Pacino | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/stylesheet.css">
</head>
<body>

<header>
    <div class="logo">
        <a href="../index.html">
            <img src="../media/logo-grof.jpg" alt="logo">
        </a>
    </div>
    <nav role="navigation">
        <div id="mmenu" class="dropdown">
            <button class="dropbtn">Menu</button>
            <div class="dropdown-content" class="font">
                <a href="../index.html">Home</a>
                <a href="mijnidool.html">Mijn idool</a>
                <a href="overmij.html">Over mij</a>
                <a href="contact.html">Contactpagina</a>
                <a href="contactformulier.php">Contactformulier</a>
            </div>
        </div>
        <div id="mitems">
            <ul>
                <li>
                    <div class="dropdown">
                        <button class="dropbtn">Contact</button>
                        <div class="dropdown-content" class="font">
                            <a href="contact.html">Contactpagina</a>
                            <a href="contactformulier.php">Contactformulier</a>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="mijnidool.html">Mijn idool</a>
                </li>
                <li>
                    <a href="overmij.html">Over mij</a>
                </li>
            </ul>
        </div>

    </nav>
</header>
<div class="container">
    <form action="" method="post">
        <?php
        if ($sentMail)
        {
            echo '<span class="bericht">Bericht is succesvol verstuurd! We nemen binnen 24 uur contact met je op.</span>';
        }
        ?>
        <div>
            <input class="form" class="left" type="email" name="email" placeholder="E-mail" size="50">
            <br>
            <textarea class="form" name="message" rows="20" cols="100" placeholder="Jouw berichtje"></textarea>
            <input style="margin-top: 20px; width: 100px;" class="form" name="submit" type="submit"
                   value="Verstuur!">
        </div>
    </form>
</div>
    <footer>
        <div id="credits">
            <p>Copyright | Christiaan Louwerman</p>
        </div>
    </footer>
</body>
</html>